call plug#begin('~/.config/nvim/autoload/plugged')

    " File Explorer
    Plug 'scrooloose/NERDTree'
    " Auto pairs for '(' '[' '{'
    Plug 'jiangmiao/auto-pairs'
    "Autocomplete lisp etc.
    Plug 'neoclide/coc.nvim', {'branch': 'release'}
    "startup screen
    Plug 'mhinz/vim-startify'
    "Theme
    Plug 'morhetz/gruvbox'
	Plug 'liuchengxu/vim-which-key'	
	"fuzzy file finder
	Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
	Plug 'junegunn/fzf.vim'

call plug#end()
