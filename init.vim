"plugin config
source $HOME/.config/nvim/vim-plug/plugins.vim
source $HOME/.config/nvim/plug-config/coc.vim


"main config
source $HOME/.config/nvim/general/settings.vim
source $HOME/.config/nvim/keys/mappings.vim

colorscheme gruvbox
set tabstop=4
set softtabstop=0 noexpandtab
set shiftwidth=4
